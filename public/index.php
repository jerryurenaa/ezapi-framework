<?php
    namespace EZAPIFRAMEWORK;
    use Core\Dispatch;
    

    /**
    * @copyright (c) Nerdtrix LLC 2021
    * @author Name: Jerry Urena
    * @author Social links:  @jerryurenaa
    * @author email: jerryurenaa@gmail.com
    * @author website: jerryurenaa.com
    * @license MIT (included with this project)
    * @comment This is the entry point of the APP, 
    * therefore we autoload every class starting from this point.
    */
        

    #Autoload
    require (
        sprintf(
            "%s%ssrc%sCore%sAutoload.php", 
            dirname(__DIR__), 
            DIRECTORY_SEPARATOR, 
            DIRECTORY_SEPARATOR, 
            DIRECTORY_SEPARATOR
        )
    );
    
    #Dispatch request
    Dispatch::request();