# EZAPI Framework 

Easy api framework is a model router service framework whose purpose is to serve a clienside app with the idea of build once deploy everywhere.

## Localhost only
- Start MySQL and Apache in Xampp or any other
- Run the following command to start the server in port 8080\
``` php -S localhost:8080 -t public public/index.php ```

## First time only
- Install xampp
- Duplicate two times and rename .env.example to .env.development and .env.production
- Add the database credentials to the .env file
- Follow **Localhost only** instructions above to start the server


## DOCUMENTATION

Please read the official documentation on our [website](https://nerdtrix.com/)

## CONTRIBUTING
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## DONATIONS



## LICENSE
EZ API Framework is [MIT](https://choosealicense.com/licenses/mit/) licensed.


###### [**Powered by Nerdtrix.com**](http://nerdtrix.com)