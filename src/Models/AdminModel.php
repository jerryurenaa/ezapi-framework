<?php
    namespace Models;
    use Core\Model;
    use Core\Logger;
    use Core\Constant;
    use Core\Globals;
    use Core\RBAC;
    use Core\Mail;
    use Core\Languages\Dictionary;
    use \Exception;

    
  class AdminModel extends Model 
  {
    public $table = null; 


    /**
     * @method listUsers
     * @param int limit, offset
     * @return object
     */
    public function listUsers(int $limit, int $offset) : object
    {
      (object) $users =  $this->db->select([
        "limit" => $limit, 
        "offset" => $offset
      ], "user");      

      #unset password for security reasons
      if(!isset($users->{0}) && !empty($users->password))
      {
        #1 result
        unset($users->password);
      }
      else if(isset($users->{0}))
      {
        #manny results
        foreach ($users as $key => $unused) 
        {
          unset($users->$key->password);
        }
      }

      /**To do: remove current logged admin from the array */

      return $users;
    }


    /**
     * @method findUser
     * @param array condition
     * @return object 
     */
    public function findUser(array $condition) : object
    {
      (object) $search = $this->find($condition, "user");

      if(!isset($search->{0}) && !empty($search->password))
      {
        unset($search->password);
      }
      else if(isset($search->{0}))
      {
        #unset password for security reasons
        foreach ($search as $key => $unused) 
        {
          unset($search->$key->password);
        }
      }

      return $search;
    }


     /**
     * @method deleteUser
     * @param int userId
     * @return string
     */
    public function deleteUser(int $userId) : string
    {
      (array) $deleteResp = $this->deleteId($userId, "user");

      if(array_key_exists("message", $deleteResp))
      {
        return $deleteResp["message"];
      }

      #Add error to logger
      Logger::write($deleteResp, 3);

      throw new Exception(serialize($deleteResp));
    }


    /**
     * @method updateUser
     * @param object post
     * @return string
     * @throws exception
     */
    public function updateUser(object $post) : string
    {
      /**
       * for security reasons the following fields can only be changed by the user
       * fName
       * Lname
       * username
       * email
       * password
       * twoFactorAuth
       */

      (array) $validation = [];

      (array) $modifiable = [
        "status",
        "role",
        "locale"
      ];
      
      if(empty($post->id)) throw new Exception($this->lang->translate("user_id_required"));

      foreach($post as $key => $ununsedValue)
      {
        if($key != "id")
        {
          if(!in_array($key, $modifiable))
          {
            $validation[$key] = $this->lang->translate("access_to_this_field_is_forbidden");
          }
        }
      }

      #verify locale
      if(!empty($post->locale))
      {
        if(!in_array($post->locale, $this->di->Dictionary->supportedLocales()))
        {
          $validation['locale'] = $this->lang->translate("invalid_language_locale");
        }
      }

      #verify status
      if(!empty($post->status))
      {

        if(!in_array($post->status, $this->db->tableType("status", "user")))
        {
          $validation['status'] = $this->lang->translate("invalid_status");
        }

        #Switch to enum key
        $post->status = array_search($post->status, $availableStatus);
      }

      #verify role
      if(!empty($post->role))
      {
        if(!in_array($post->role, RBAC::roles()))
        {
          $validation['role'] = $this->lang->translate("invalid_role");
        }
      }
      
      #Errors
      if(!empty($validation)) throw new Exception (serialize($validation));

      #User id
      (int) $userId = $post->id;

      #unsetId
      unset($post->id);

      $updateResp = $this->updateById($userId, (array) $post, "user");

      if($updateResp == Constant::SUCCESS)
      {
        return $updateResp;
      }

      #Error
      throw new Exception ($updateResp);
    }

    
    /**
     * @method addUser
     * @param object post
     * @return string
     * @throws exception
     */
    public function addUser(object $post) : string
    {
      $validation = [];

      #STEP1 VALIDATE EMPTY FIELDS
      if(empty($post->fName)) $validation["fName"] = $this->lang->translate("first_name_is_required");
      if(empty($post->lName)) $validation["lName"] =  $this->lang->translate("last_name_is_required");
      if(empty($post->username)) $validation["username"] =  $this->lang->translate("username_is_required");
      if(empty($post->email)) $validation["email"] = $this->lang->translate("email_is_required");
      if(empty($post->password)) $validation["password"] =  $this->lang->translate("password_is_required");      
      
      if(!empty($validation)) throw new Exception (serialize($validation));

      #STEP2 EMAIL VALIDATION
      if(!filter_var($post->email, FILTER_VALIDATE_EMAIL)) $validation["email"] = $this->lang->translate("invalid_email_address");
      
      if(!empty($validation)) throw new Exception (serialize($validation));

      $user = $this->findFirst(["email" => $post->email], "user");
      if(!empty($user->email)) $validation["email"] = $this->lang->translate("this_email_already_exists");
      
      if(!empty($validation)) throw new Exception (serialize($validation));

      #STEP3 USERNAME VALIDATION
      if(strlen($post->username) < 8) $validation["username"] = $this->lang->translate("your_username_is_too_short");
      if(strlen($post->username) > 150) $validation["username"] = $this->lang->translate("your_username_is_too_long");
      
      if(!empty($validation)) throw new Exception (serialize($validation));

      $user = $this->findFirst(["username" => $post->username], "user");
      if(!empty($user->username)) $validation["username"] = $this->lang->translate("this_username_already_exists");
      
      if(!empty($validation)) throw new Exception (serialize($validation));

      #STEP4 PASSWORD VALIDATION
      if(strlen($post->password) < 8) $validation["password"] =  $this->lang->translate("your_password_is_too_short");
      
      #ADD ANY OTHER PASSWORD VALIDATIONS HERE

      if(!empty($validation)) throw new Exception (serialize($validation));

      #validate user preference
      if(!empty($post->twoFactorAuth) && $post->twoFactorAuth != 1)
      {
        $validation["twoFactorAuth"] =  $this->lang->translate(""); //Invalid value
      }

      #validate user's role
      if(!empty($post->role) && !in_array($post->role, RBAC::roles()))
      {
        $validation["role"] =  $this->lang->translate(""); //Invalid value
      }

      #validate user status
      if(!empty($post->status) && !in_array($post->status, $this->db->tableType("status", "user")))
      {
        $validation["status"] =  $this->lang->translate(""); //Invalid value
      }
     
      if(!empty($post->locale) && !in_array($post->locale, $this->di->Dictionary->supportedLocales()))
      {
        $validation["locale"] =  $this->lang->translate(""); //Invalid value
      }

      if(!empty($validation)) throw new Exception (serialize($validation));

      #Assign default value
      if(empty($post->twoFactorAuth)) $post->twoFactorAuth = 0; #inactive

      #Assign default value
      if(empty($post->role)) $post->role = "USER";

      #Assign default value
      if(empty($post->status)) $post->status = 2; #active     

      #Assign default value
      if(empty($post->locale)) $post->locale = EZENV["DEFAULT_LANGUAGE_LOCALE"];
      
      #save user
      if($this->saveValues((array) $post, "user"))
      {
        #send welcome email
        if(!empty($post->sendWelcomeEmail) && $post->sendWelcomeEmail)
        {
          #Send welcome email First
          Mail::sendWelcomeEmail($post->fName, $post->email);
        }

        return Constant::SUCCESS;
      }
     
      throw new Exception (Constant::ERROR_MESSAGE);
    }


    /**
     * @method totalLogged
     * @return int
     */
    public function totalLogged() : int
    {
      #Custom DB query
      (object) $session = $this->db->select([
        "where" => "expire >= ?",
        "bind" => [CURRENT_TIME]
      ], "session");

      #current logged user does not count
      if(!empty($session->expire))
      {
        return 0;
      }

      #Count session and discount the current logged Admin
      (int) $total = count((array) $session) - 1;
    
      #Return total logged
      return $total;
    }
    

    /**
     * @method listRoles
     * @param int limit, offset
     * @return object
     */
    public function listRoles(int $limit, int $offset) : object
    {
      (object) $listRoles = $this->db->select([
        "limit " => $limit, 
        "offset" => $offset
      ], "rbac");

      if(!isset($listRoles->{0})) throw new Exception(Constant::ERROR_MESSAGE);

      return $listRoles;
    }

    
    /**
     * @method addRole
     * @param object post
     * @return string
     * @throws exception
     */
    public function addRole(object $post) : string
    {
      $validation = [];

      #STEP1 VALIDATE EMPTY FIELDS
      if(empty($post->route)) $validation["route"] = $this->lang->translate("a_route_is_required");
      if(empty($post->action)) $validation["action"] =  $this->lang->translate("at_least_one_action_is_required");
      if(empty($post->role)) $validation["role"] =  $this->lang->translate("a_role_is_required");
      if(empty($post->permission)) $validation["permission"] = $this->lang->translate("at_least_one_permission_must_be_set");
      //$post->description is obtional
         
      if(!empty($validation)) throw new Exception (serialize($validation));

      #validate user's role
      if(Globals::$userRole != "ADMIN")
      {
        throw new Exception ($this->lang->translate("you_do_not_have_permissions"));
      }
      
      #save role
      if($this->saveValues((array) $post, "rbac"))
      {
        return Constant::SUCCESS;
      }
     
      throw new Exception (Constant::ERROR_MESSAGE);
    }

    public function editRole(object $post) : string
    {
      (array) $validation = [];
      
      #Validate id
      if(empty($post->id)) throw new Exception($this->lang->translate("user_id_required"));      
      
      #Errors
      if(!empty($validation)) throw new Exception (serialize($validation));

      #User id
      (int) $userId = $post->id;

      #unsetId from the object
      unset($post->id);

      $updateResp = $this->updateById(
        $userId, 
        (array) $post, 
        "rbac" //Table name
      );

      if($updateResp == Constant::SUCCESS)
      {
        return $updateResp;
      }

      #Error
      throw new Exception ($updateResp);
    }


    /**
     * @method deleteRole
     * @param int id
     * @return string
     * @throws exception
     */
    public function deleteRole(int $roleId) : string
    {
      #Delete Id
      (array) $deleteResp = $this->deleteId($roleId, "rbac");

      if(array_key_exists("message", $deleteResp))
      {
        return $deleteResp["message"];
      }

      #Add error to logger
      Logger::write($deleteResp, 3);


      throw new Exception(serialize($deleteResp));
    }
    
  }