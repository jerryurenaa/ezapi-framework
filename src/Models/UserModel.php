<?php
    namespace Models;
    use Core\Model;
    use Core\Session;
    use Core\Globals;
    use Core\Constant;
    use Core\Mail;
    use Core\Token;
    use Core\Logger;
    use Core\Languages\Dictionary;
    use \Exception;

    
  class UserModel extends Model 
  {
    /**
     * The names here are case sensitive. These names must match the names in the database table.
     * table variable must always be present.
     * if no table is required set table to null
     */
    public 
      $table = "user", 
      $id, #int 11
      $fName, #varchar 
      $lName, 
      $username, 
      $email, 
      $password, 
      $twoFactorAuth = EZENV["USE_TWO_FACTOR_BY_DEFAULT"] ? 1 : 0, #disable by default
      $status = !EZENV["ENFORCE_EMAIL_VALIDATION"] ? 2 : 1, #Inactive by default
      $role = "USER",
      $locale = EZENV["DEFAULT_LANGUAGE_LOCALE"], 
      $created;

   


    /**
     * @method login
     * @param object username_or_email, password
     * @return string
     * @throws exception
     */
    public function login(object $post) : string 
    {
      #Check if username or email field is empty
      if(empty($post->username_or_email))
      {
        throw new Exception (serialize(["username_or_email" => $this->lang->translate("username_or_email_is_required")]));
      }

      #Check if password field is empty
      if(empty($post->password))
      {
        throw new Exception (serialize(["password" => $this->lang->translate("password_is_required")]));
      }

      #Check whether the user entered a valid email otherwise treat it as an username
      $identifier = filter_var($post->username_or_email, FILTER_VALIDATE_EMAIL) ? "email" : "username";
      
      #Attempt to find the user in the database with the username or email provided
      $user =  $this->findFirst([$identifier => $post->username_or_email]);
 
      #User not found
      if(empty($user->$identifier))
      {
        throw new Exception (serialize(["username_or_email" => $this->lang->translate("invalid_username_or_email")]));
      }
      
      #Invalid password
      if(!password_verify($post->password, $user->password))
      {
        throw new Exception (serialize(["password" => $this->lang->translate("invalid_password")]));
      }

      #if user is banned do not continue
      if($user->status == "banned")
      {
        throw new Exception (serialize(["banned" => $this->lang->translate("account_banned")]));
      }

      if($user->status == "inactive")
      {
        throw new Exception (serialize(["inactive" => $this->lang->translate("account_inactive")]));
      }

      #Asign values
      $this->assign($user);
      
      /**
       * If enforce email validation is enable and the account status is inactive return OTP required or
       * if the account has 2 factor auth enable send an OPT. 
       */
      if($this->status == "inactive" && EZENV["ENFORCE_EMAIL_VALIDATION"] || $user->twoFactorAuth)
      {
        #OTP required
        if(empty($post->token))
        {
          #Send OTP
          Mail::sendOtpToken($this->fName, $this->email, Token::get($this->id)); 
          
          throw new Exception (serialize(["token" =>  $this->lang->translate("OTP_SENT")]));
        }

        #validate token auth
        $tokenResp = Token::validate($this->id, $post->token);

        if($tokenResp != constant::SUCCESS)
        {
          throw new Exception (serialize(["token" => $tokenResp]));
        }

        #activate account
        if($this->status == "inactive")
        {
          #update account status
          $update = $this->updateId(["status" => 2]);

          #Send welcome email First
          Mail::sendWelcomeEmail($this->fName, $this->email);
        }
      }

      #asign cookie session
      $handleSession = Session::set($this->id);

      if($handleSession == Constant::SUCCESS)
      {
        #unset password
        $this->password = null;

        #Set global
        Globals::$userId = $this->id;
        Globals::$userRole = $this->role;
        Globals::$userLanguage = $this->locale;
        
        #change language preference
        if($this->lang->currentLocale() != $this->locale)
        {
          $this->lang->setLocale($this->locale);
        }

        return Constant::SUCCESS;
      }

      #Add error to logger
      Logger::write($handleSession, 3);

      #Unable to set cookie session
      throw new Exception (Constant::ERROR_MESSAGE);
    }


    /**
     * @method register
     * @param object 
     * @return string
     * @throws exception
     */
    public function register(object $post) : string
    {
      $validation = [];

      #STEP1 VALIDATE EMPTY FIELDS
      if(empty($post->fName)) $validation["fName"] = $this->lang->translate("first_name_is_required");
      if(empty($post->lName)) $validation["lName"] =  $this->lang->translate("last_name_is_required");
      if(empty($post->username)) $validation["username"] =  $this->lang->translate("username_is_required");
      if(empty($post->email)) $validation["email"] = $this->lang->translate("email_is_required");
      if(empty($post->password)) $validation["password"] =  $this->lang->translate("password_is_required");
      if(empty($post->confirmPassword)) $validation["confirmPassword"] = $this->lang->translate("password_confirmation_is_required");
      
      if(!empty($validation)) throw new Exception (serialize($validation));

      #STEP2 EMAIL VALIDATION
      if(!filter_var($post->email, FILTER_VALIDATE_EMAIL)) $validation["email"] = $this->lang->translate("invalid_email_address");
      
      if(!empty($validation)) throw new Exception (serialize($validation));

      $user = $this->findFirst(["email" => $post->email]);
      if(!empty($user->email)) $validation["email"] = $this->lang->translate("this_email_already_exists");
      
      if(!empty($validation)) throw new Exception (serialize($validation));

      #STEP3 USERNAME VALIDATION
      if(strlen($post->username) < 8) $validation["username"] = $this->lang->translate("your_username_is_too_short");
      if(strlen($post->username) > 150) $validation["username"] = $this->lang->translate("your_username_is_too_long");
      
      if(!empty($validation)) throw new Exception (serialize($validation));

      $user = $this->findFirst(["username" => $post->username]);
      if(!empty($user->username)) $validation["username"] = $this->lang->translate("this_username_already_exists");
      
      if(!empty($validation)) throw new Exception (serialize($validation));

      #STEP4 PASSWORD VALIDATION
      if(strlen($post->password) < 8) $validation["password"] =  $this->lang->translate("your_password_is_too_short");
      if($post->password != $post->confirmPassword) $validation["password"] = $this->lang->translate("your_password_does_not_match");
      
      #ADD ANY OTHER PASSWORD VALIDATIONS HERE

      if(!empty($validation)) throw new Exception (serialize($validation));

      #assign values
      $this->assign($post);

      #save record
      if(!$this->save())
      {
        throw new Exception (Constant::ERROR_MESSAGE);
      }

      #Assign user id
      $this->id = $this->db->lastInsertedId();
          
      #By default email validation is enable
      if(EZENV["ENFORCE_EMAIL_VALIDATION"])
      {
        #Send OTP email
        Mail::sendOtpToken(
          $this->fName,
          $this->email,
          Token::get($this->id) #Get a new token
        );
        
        #Registration susccessful, now Validate the OTP token
        return Constant::OTP_SENT;
      }

      #Registration susccessful
      return Constant::SUCCESS;
    }


    /**
     * @method updateId
     * @param array
     * @return string
     */
    public function updateId(array $array) : string
    {
      return $this->updateById($this->id, $array);
    }
   

    /**
     * @method exists
     * @param string username_or_email
     * @return int
     */
    public function exists(string $username_or_email) : int
    {
      #If the user entered a valid email we will search the db with an email for everything else we will use username
      $findBy = filter_var($username_or_email, FILTER_VALIDATE_EMAIL) ? "email" : "username";

      $user = $this->findFirst([$findBy => $username_or_email]);
      
      if(!empty($user))
      {
        #Assign user values
        $this->assign($user);

        return $user->id;
      }
    }

    
    /**
     * @method isLogged
     * @return bool
     */
    public function isLogged() : bool
    {
      #If the cookie is not found, auth is required
      if(!$cookieUserId = Session::get()) return false;

      $user = $this->findFirst(["id" => $cookieUserId]);

      #It will always be true but we must have some kind of validation here
      if(!empty($user))
      {
        $this->assign($user);

        #Set global
        Globals::$userId = $this->id;
        Globals::$userRole = $this->role;
        Globals::$userLanguage = $this->locale;

        return true;
      }

      return false;
    }


    /**
     * @method info
     * @return array
     */
    public function info() : array
    {
      return $this->vars();
    }


    /**
     * @method resetPsw
     * @param object post
     * @return string
     * @throws exception
     */
    public function resetPsw(object $post) : string
    {
      $validation = [];

      #send OTP
      if(!$this->isLogged() && empty($post->token))
      {
        #Username or email is requried
        if(empty($post->username_or_email)) $validation['username_or_email'] = $this->lang->translate("username_or_email_is_required");
       
        #find user with user email
        $userId = $this->exists($post->username_or_email);

        if (!$userId)
        {
          $validation['username_or_email'] = $this->lang->translate("invalid_username_or_email");
        }

        #Send OTP email
        Mail::sendOtpToken(
          $this->fName,
          $this->email,
          Token::get($this->id) #Get a new token
        );

        #Token sent
        return Constant::OTP_SENT;
      }

      #Fields validations
      if(empty($post->password)) $validation["password"] = $this->lang->translate("password_is_required");
      if(strlen($post->password) < 8) $validation['password'] =  $this->lang->translate("your_password_is_too_short");
      if(strlen($post->password) > 150) $validation['password'] =  $this->lang->translate("your_password_is_too_long");

      #Add any other password validations here

      if(empty($post->confirmPassword)) $validation["confirmPassword"] = $this->lang->translate("password_confirmation_is_required");
      if(!empty($post->confirmPassword) && $post->password !=  $post->confirmPassword) $validation['confirmPassword'] =  $this->lang->translate("your_password_confirmation_doesnt_match");

      #Validation errors
      if(!empty($validation)) throw new Exception (serialize($validation));

      #If the user is logged the current password is requried to validate the change
      if(!empty($this->id))
      {
        if(empty($post->currentPassword)) $validation["currentPassword"] = $this->lang->translate("current_password_password");
        
        #verify  if the password match
        if(!password_verify($post->currentPassword, $this->password))
        {
          $validation["currentPassword"] = $this->lang->translate("current_password_is_invalid");
        }
      }
      else
      { 
        #Username or email is requried
        if(empty($post->username_or_email)) $validation['username_or_email'] = $this->lang->translate("username_or_email_is_required");
       
        #find user with user email
        $userId = $this->exists($post->username_or_email);

        if (!$userId)
        {
          $validation['username_or_email'] = $this->lang->translate("invalid_username_or_email");
        }

        #Call validation errors again
        if(!empty($validation)) throw new Exception (serialize($validation));
        
        #Validate token
        $validate = Token::validate($userId, $post->token);

        if($validate != Constant::SUCCESS)
        {
          $validation['token'] = $validate;
        }
      }

      #Call validation errors again
      if(!empty($validation)) throw new Exception (serialize($validation));

      #Update password field
      $updateResp = $this->updateById($this->id, ["password" => $post->password]);

      if($updateResp == Constant::SUCCESS)
      {
        #Send confirmation
        Mail::resetPsw($this->fName, $this->email);

        return Constant::SUCCESS;
      }

      #Add error to logger
      Logger::write($updateResp, 3);

      #Update password
      return Constant::ERROR;
    }


    /**
     * @method update
     * @param object
     * @return string
     * @throws exception
     */
    public function update(object $post) : string
    {
      $validation = [];

      if(!$this->isLogged()) throw new Exception (Constant::ERROR_MESSAGE);

      #EMAIL VALIDATION
      if(!empty($post->email))
      {
        if(!filter_var($post->email, FILTER_VALIDATE_EMAIL)) $validation['email'] = $this->lang->translate("invalid_email_address");
        
        $user = $this->findFirst(["email" => $post->email]);
        if(!empty($user->email)) $validation['email'] = $this->lang->translate("this_email_already_exists");
      }

       #USERNAME VALIDATION
      if(!empty($post->username))
      {
        if(strlen($post->username) < 8) $validation['username'] = $this->lang->translate("your_username_is_too_short");
        if(strlen($post->username) > 150) $validation['username'] = $this->lang->translate("your_username_is_too_long");

        $user = $this->findFirst(["username" => $post->username]);
        if(!empty($user->username)) $validation['username'] = $this->lang->translate("this_username_already_exists");
      }

      #update password
      if(!empty($post->password))
      {
        if(empty($post->currentPassword)) $validation["currentPassword"] = $this->lang->translate("current_password_password");
        
        #return errors
        if(!empty($validation)) throw new Exception (serialize($validation));

        #verify  if the password match
        if(!password_verify($post->currentPassword, $this->password))
        {
          $validation["currentPassword"] = $this->lang->translate("current_password_is_invalid");
        }

        #Fields validations
        if(empty($post->password)) $validation["password"] = $this->lang->translate("password_is_required");
        if(strlen($post->password) < 8) $validation['password'] =  $this->lang->translate("your_password_is_too_short");
        if(strlen($post->password) > 150) $validation['password'] =  $this->lang->translate("your_password_is_too_long");

        #Add any other password validations here

        if(empty($post->confirmPassword)) $validation["confirmPassword"] = $this->lang->translate("password_confirmation_is_required");
        if(!empty($post->confirmPassword) && $post->password !=  $post->confirmPassword) $validation['confirmPassword'] =  $this->lang->translate("your_password_confirmation_doesnt_match");

        #return errors
        if(!empty($validation)) throw new Exception (serialize($validation));

        #remove current password and confirm password from the object since they were only needed for validation
        unset($post->currentPassword);
        unset($post->confirmPassword);
      }

      #Only admins can update these columns. We also blacklist ID and created to prevent security leaks
      if(!empty($post->role) || !empty($post->status) || !empty($post->created) || !empty($post->id) || isset($post->id))
      {
        $validation['role'] =  $this->lang->translate("access_to_this_field_is_forbidden");
      }

      #language
      if(!empty($post->locale))
      {
        if(!in_array($post->locale, $this->di->Dictionary->supportedLocales()))
        {
          $validation['locale'] = $this->lang->translate("invalid_language_locale");
        } 
      }

      if(!empty($validation)) throw new Exception (serialize($validation));

      #update values using UserID and params provided
      $updateResp = $this->updateId((array) $post);

      if($updateResp == Constant::SUCCESS)
      {
        return $updateResp;
      }

      #Error
      throw new Exception ($updateResp);
    }


  }
