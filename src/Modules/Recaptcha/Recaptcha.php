<?php 
    namespace Modules\Recaptcha;
    use \Exception;

class Recaptcha
{
    private $RECAPTCHA_SECRETKEY = "";
    /**
     * This function will get the user recaptcha and validate it with google server to prevent bots
     * recaptcha V2.0
     * @method validateRecaptcha
     * @param string $recaptcha 
     * @return string
     */
    public static function validateRecaptcha($code)
    {
        $dataArr = [
            'secret' => $this->RECAPTCHA_SECRETKEY, 
            'response' => $code, //required
            'remoteip'=> $_SERVER['REMOTE_ADDR'] //optional field
        ];

        $url = "https://www.google.com/recaptcha/api/siteverify";

        try
        {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($dataArr));
            
            $response = curl_exec($curl);

            //get error code
            $responseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            
            if ($responseCode >= 400) 
            {
                throw new Exception ("Response error code: {$responseCode}");
            }

            $response = json_decode($response, true);     
            
            if ($response['success'] === true) 
            {
                // Success
                return "Verified";
            }

            throw new Exception ("verification failed");
        }
        catch(Exception $e)
        {
            return $e->getMessage();
        }
    }
}