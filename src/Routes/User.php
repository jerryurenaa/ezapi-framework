<?php
  namespace Routes;
  use Core\Router;
  use Models\UserModel;
  use Core\Helper;
  use Core\Session;
  use Core\Constant;
  use Core\Mail;
  use Core\Token;
  use \Exception;

  /**
  * @copyright (c) Nerdtrix LLC 2021
  * @author Name: Jerry Urena
  * @author Social links:  @jerryurenaa
  * @author email: jerryurenaa@gmail.com
  * @author website: jerryurenaa.com
  * @license MIT (included with this project)
  */

  
  class User extends Router
  {

    #Constructor
    public function __construct()
    {
      parent::__construct();
    }


    /**
     * @method index (Default)
     * @return object
     * @comment: Basic app informaiton.
     */
    public function index() : void
    {
      $this->request->response(200, [
        "greeting" => $this->lang->translate("hello_world"), 
        "default_language" => EZENV["DEFAULT_LANGUAGE_LOCALE"], 
        "server_version"=> EZENV["APP_VERSION"], 
        "supported_languages" => $this->lang->list()
      ]);
    }


    /**
     * @method setLanguageLocale
     * @param  object ex: {"locale": "es_US"} 
     * @return object
     * @throws Exceptions
     * @comment: Call this method if you would like to receive responses in a different language. 
     * This function will prevent you from having to translate messages manually in 
     * frontend with multilingual applications. EX: if the default language is english (en_US)
     * but the user changed the language locale to spanish (es_US) the response message will be 
     * "Usuario invalido" instead of "Invalid username".
     */
    public function setLanguageLocale() : void
    {
      try
      {
        #Receive params and sanitize
        $post = $this->request->inputJson(true);

        #Set language locale preference
        $setLocale = $this->lang->setLocale($post->locale);

        if($setLocale !== Constant::SUCCESS)
        {
          throw new Exception ($setLocale);
        }

        #By default if the user is logged we will also update the preference in the database
        if($this->di->UserModel->isLogged())
        {
          $errors = $this->di->UserModel->updateById($this->di->UserModel->id, ["locale" => $post->locale]);

          if($errors !== Constant::SUCCESS)
          {
            throw new Exception ($errors);
          }
        }

        #Operation successful
        $this->request->response(200, [
          Constant::MESSAGE =>  Constant::SUCCESS_MESSAGE
        ]);
      }
      catch(Exception $ex)
      {
        $this->exceptionHandler($ex);
      }
    } 

    /**
     * @method listLanguages
     * @return object
     * @throws exception
     */
    public function listLanguages() : void
    {
      try
      {
        $this->request->response(200, [
          "default_language" => EZENV["DEFAULT_LANGUAGE_LOCALE"],
          "supported_languages" => $this->lang->list()
        ]);
      }
      catch(Exception $ex)
      {
        $this->exceptionHandler($ex);
      }
    }


    /**
     * @method login
     * @param object ex: {username_or_email: string value, password: string value, token: int value}
     * @return object
     * @throws Exceptions
     */
    public function login() : void
    {
      try
      {
        #If the user is logged there is no need to continue
        if($this->di->UserModel->isLogged())
        {
          $this->request->response(200, $this->di->UserModel->info());
        }

        #Receive params and sanitize them.
        $post = $this->request->inputJson(true);

        #Attempt to login
        $response = $this->di->UserModel->login($post);
        
        #logged in successfully
        if($response == Constant::SUCCESS)
        {
          $this->request->response(200, $this->di->UserModel->info());
        }

        #Invalid auth
        throw new Exception (serialize($response));
      }
      catch (Exception $ex)
      {
        $this->exceptionHandler($ex);
      }
    }


    /**
     * @method register
     * @param object ex:  {fname: value, lname: value, username: value, email: value, password: value, language: value}
     * @return object
     * @throws exceptions 
     * @Comment: After all fields are validated, by default an otp code will be sent to the user's email.
     * To activate an account call the route validateOTP with the token. If you would like to disable email validation
     * you can do so by disabling the ENFORCE_EMAIL_VALIDATION env.
     * 
     */
    public function register() : void
    {
      try
      {
        if(!EZENV["ALLOW_REGISTRATION"])
        {
          throw new Exception ($this->lang->translate("registrations_are_currently_disabled"));
        }

        #If the user is logged DO NOT CONTINUE
        if($this->di->UserModel->isLogged())
        {
          throw new Exception ($this->lang->translate("please_logout_and_try_again"));
        }
        
        #Receive params and sanitize them to continue
        $post = $this->request->inputJson(true);
        
        #Attempt to Register
        $response = $this->di->UserModel->register($post);
        
        #Registration successful but an OTP is required to activate the account
        if($response == Constant::OTP_SENT)
        {
          $this->request->response(200, ["token" => $response]);
        }
        
        #Registration successful
        if($response == Constant::SUCCESS)
        {
          $this->request->response(200, [Constant::MESSAGE =>  $response]);
        }

        #Invalid params
        throw new Exception (serialize($response));
      }
      catch(Exception $ex)
      {
        $this->exceptionHandler($ex);
      }
    }

    
    /**
     * @method activateAccount
     * @param object {username_or_email: string value, token: int value}
     * @return object
     * @throws exception
     */
    public function activateAccount() : void
    {
      try
      {
        #Receive params and sanitize them to continue
        $post = $this->request->inputJson(true); 

        #If user exists continue with OTP validation
        $userId = $this->di->UserModel->exists($post->username_or_email);

        if(empty($userId))
        {
          throw new Exception (serialize(["username_or_email" => $this->lang->translate("invalid_username_or_email")]));
        }

        #Verify if the account is active or not
        if($this->di->UserModel->status == "active")
        {
          throw new Exception ($this->lang->translate("this_account_is_already_activated"));
        }

        #Validate OTP
        $tokenResp = Token::validate($userId, $post->token);

        if($tokenResp != constant::SUCCESS)
        {
          throw new Exception (serialize(["token" => $tokenResp]));
        }

        #update user status from inactive to active
        $updateResp = $this->di->UserModel->updateId(["status" => 2]);               
        
        #Registration successful
        if($updateResp == Constant::SUCCESS)
        {
          #Send welcome email First
          Mail::sendWelcomeEmail($this->di->UserModel->fName, $this->di->UserModel->email);

          #All done
          $this->request->response(200, [Constant::MESSAGE =>  $updateResp]);
        }

        #return a fancy error
        throw new Exception (Constant::ERROR_MESSAGE);
      }
      catch(Exception $ex)
      {
        $this->exceptionHandler($ex);
      }
    }

    
    /**
     * @method info
     * @return object 
     * @throws exception
     * @comment: This method will return the current logged in user information.
     */
    public function info() : void
    {
      try 
      {
        #IsLogged will assign the current user info
        if($this->di->UserModel->isLogged())
        {
          $this->request->response(200, $this->di->UserModel->info());
        } 

        #return a fancy error
        throw new Exception (Constant::ERROR_MESSAGE);
      } 
      catch (Exception $ex) 
      {
        $this->exceptionHandler($ex);
      }     
    }


    /**
     * @method logout
     * @return object 
     * @throws exception
     * @comment: This method can be called by post or get there is no difference.
     */
    public function logout() : void
    {
      try 
      {
        #Distroy session
        $status = Session::delete();

        if($status == Constant::SUCCESS)
        {
          #response
          $this->request->response(200, [
            Constant::MESSAGE =>  Constant::SUCCESS_MESSAGE
          ]);
        }

        #return a fancy error
        throw new Exception (Constant::ERROR_MESSAGE);
      } 
      catch (Exception $ex) 
      {
        $this->exceptionHandler($ex);
      } 
    }


    /**
     * @method extend
     * @return object 
     * @throws exception
     */
    public function extend() : void
    {
      try 
      {
        if(Session::extend())
        {
          $this->request->response(200, [
            Constant::MESSAGE =>  Constant::SUCCESS_MESSAGE
          ]);
        }

        #return a fancy error
        throw new Exception (Constant::ERROR_MESSAGE);
      } 
      catch (Exception $ex) 
      {
        $this->exceptionHandler($ex);
      } 
    } 
    
    
    /**
     * @method resetPsw
     * @param object 
     * @return object
     * @throws exception
     * comment: this route works for both guest and logged users.
     * for guest users a token will be sent to their email. call this route with {username_or_email: value} to get a token
     * once you get a token you can call this route with all the fields to reset the password ex: 
     * {username_or_email: string value, token: string value, password: string value, confirmPassword: string value}
     * 
     * if the user is logged you can call this route without the token but instead a current password is requried ex
     * {currentPassword: string value, password: string value, confirmPassword: string value}
     */
    public function resetPsw() : void
    {
      try
      {
        #Receive params and sanitize them
        $post = $this->request->input(true);
       
        #Call reset psw model
        $resetPSWResp = $this->di->UserModel->resetPsw($post);

        #Password reset successful
        if($resetPSWResp == Constant::SUCCESS)
        {
          $this->request->response(200, [Constant::MESSAGE =>  Constant::SUCCESS_MESSAGE]);
        }

        #OTP SENT
        if($resetPSWResp == Constant::OTP_SENT)
        {
          $this->request->response(200, ["Token" =>  $this->lang->translate("OTP_SENT")]);
        }

        #Error
        throw new Exception ($resetPSWResp);
      }
      catch (Exception $ex)
      {
        $this->exceptionHandler($ex);
      }
    }


    /**
     * @method update
     * @param POST
     * @return object (JSON)
     * @throws exception
     * @comment: update one or many fields
     */
    public function update() : void
    {
      try
      {
        #Receive params and sanitize them before next step
        $post = $this->request->input(true);

        #Validate required params
        $updateResp = $this->di->UserModel->update($post);

        if($updateResp == Constant::SUCCESS)
        {
          $this->request->response(200, [Constant::MESSAGE =>  Constant::SUCCESS_MESSAGE]);
        }
        
        #Error
        throw new Exception (Constant::ERROR_MESSAGE);
      }
      catch (Exception $ex)
      {
        $this->exceptionHandler($ex);
      }
    }
  }