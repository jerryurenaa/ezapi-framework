<?php
    namespace Routes;
    use Core\Router;
    use Core\Globals;
    use Core\Constant;
    use Core\RBAC;
    use Models\AdminModel;
    use \Exception;

    
    class Admin extends Router
    {
        public function __construct()
        {
            parent::__construct();
        }


        /**
         * @method listUsers
         * @param object ex: {limit: int value, offset: int value}
         * @return object
         * @throws exception
         */
        public function listUsers() : void
        {
            (int) $offset = 0;
            (int) $limit = 30;
           
            try
            {
                (object) $post = $this->request->inputJson(true);

                if(!empty($post->limit))
                {
                    $limit = (int) $post->limit;
                }

                if(!empty($post->offset))
                {
                    $offset = (int) $post->offset;
                }

                (object) $list = $this->di->AdminModel->listUsers($limit, $offset);
                
                $this->request->response(200, (array) $list);
            }
            catch (Exception $ex)
            {
                $this->exceptionHandler($ex);
            }
        }


        /**
         * @method findUser
         * @param object {id: int value} or {email: string value} or {username: string value}
         * @return object
         * @throws exception
         */
        public function findUser() : void
        {
            (array) $searchBy = null;

            try
            {
                (object) $post = $this->request->inputJson(true);

                if(!empty($post->userId)) $searchBy["id"] = $post->userId;
                if(!empty($post->email)) $searchBy["email"] = $post->email;
                if(!empty($post->username)) $searchBy["username"] = $post->username;

                if(empty($searchBy))
                {
                    throw new Exception($this->lang->translate("please_specify_a_keyword"));
                }

                (object) $list = $this->di->AdminModel->findUser($searchBy);

                if(!empty($list->id) || !empty($list->{0}->id))
                {
                    $this->request->response(200, (array) $list);
                }

                throw new Exception($this->lang->translate("there_are_no_results"));
            }
            catch (Exception $ex)
            {
                $this->exceptionHandler($ex);
            }
        }


        /**
         * @method deleteUser
         * @param object {id: int value}
         * @return object
         * @throws exception
         */
        public function deleteUser() : void
        {
            try
            {
                (object) $post = $this->request->inputJson(true);

                if(empty($post->id))
                {
                   throw new Exception($this->lang->translate("user_id_required"));
                }
    
                if($post->id == Globals::$userId)
                {
                    throw new Exception($this->lang->translate("you_cannot_delete_yourself"));
                }
    
                (string) $deleteResponse = $this->di->AdminModel->deleteUser($post->id);

                $this->request->response(200, [Constant::MESSAGE => $deleteResponse]);
            }
            catch (Exception $ex)
            {
                $this->exceptionHandler($ex);
            }
        }


        /**
         * @method updateUser
         * @param object {id: int value, fieldName: value} available changeable are status, role, locale
         * @return object
         * @throws exception
         */
        public function updateUser() : void
        {
            try
            {
                (object) $post = $this->request->inputJson(true);

                (string) $updateResp = $this->di->AdminModel->updateUser($post);

                if($updateResp == Constant::SUCCESS)
                {
                    $this->request->response(200, [Constant::MESSAGE => Constant::SUCCESS_MESSAGE]);
                }
               
                throw new Exception(Constant::ERROR_MESSAGE);
            }
            catch (Exception $ex)
            {
                $this->exceptionHandler($ex);
            }
        }


        /**
         * @method addUser
         * @param object 
         * @return object
         * @throws exception
         */
        public function addUser() : void
        {
            try
            {
                #Receive params and sanitize them
                (object) $post = $this->request->inputJson(true);

                #Call adminModel
                (string) $addResp = $this->di->AdminModel->addUser($post);

                if($addResp == Constant::SUCCESS)
                {
                    $this->request->response(200, [Constant::MESSAGE => Constant::SUCCESS_MESSAGE]);
                }
               
                throw new Exception(Constant::ERROR_MESSAGE);
            }
            catch (Exception $ex)
            {
                $this->exceptionHandler($ex);
            }
        }


        /**
         * @method totalLoggedUsers
         * @return object
         * @throws exception
         */
        public function totalLoggedUsers() : void
        {
            try
            {
                $count = $this->di->AdminModel->totalLogged();
                
                $this->request->response(200, ["count" => $count]);
            }
            catch (Exception $ex)
            {
                $this->exceptionHandler($ex);
            }
        }


        /**
         * @method listRoles
         * @param object ex: {limit: int value, offset: int value}
         * @return object
         * @throws exception
         */
        public function listRoles() : void
        {
            (int) $offset = 0;
            (int) $limit = 30;
           
            try
            {
                #Receive params
                (object) $post = $this->request->inputJson(true);

                if(!empty($post->limit))
                {
                    $limit = $post->limit;
                }

                if(!empty($post->offset))
                {
                    $offset = $post->offset;
                }

                #List roles
                (object) $list = $this->di->AdminModel->listRoles($limit, $offset);
                
                $this->request->response(200, (array) $list);
            }
            catch (Exception $ex)
            {
                $this->exceptionHandler($ex);
            }
        }


        /**
         * @method listAvailableRoles
         * @method get
         * @return object
         */
        public function listAvailableRoles() : void 
        {
            (array) $routes = [];
            (array) $respArray = [];

            #Scan routes dir
            $files = scandir(__DIR__);

            #clean names
            foreach($files as $file)
            { 
                #remove extension
                (string) $name = pathinfo($file, PATHINFO_FILENAME);

                #remove periods and spaces
                $name = str_replace(array('.', ' '), '' , $name);

                if(!empty($name))
                {
                    array_push($routes, $name);
                }
            } 

            #get name methods
            foreach($routes as $route)
            {
                #get method list
                $methodList = get_class_methods(sprintf("Routes%s%s", SLASH, $route));

                foreach($methodList as $method)
                {
                    #remove constructor and destructor
                    if($method != "__construct" && $method != "__destruct" && $method != "exceptionHandler")
                    {
                        $respArray[$route][] = $method;
                    }
                }
            }

            $this->request->response(200, $respArray);
        }


        /**
         * @method addRole
         * @method post
         * @return object
         * @throws exception
         */
        public function addRole() : void
        {
            try
            {
                #Receive params
                (object) $post = $this->request->inputJson(true);
                
                #Add role
                (string) $addResponse = $this->di->AdminModel->addRole($post);
                
                #Body response
                $this->request->response(200, [Constant::MESSAGE => $addResponse]);
            }
            catch (Exception $ex)
            {
                $this->exceptionHandler($ex);
            }
        }

        /**
         * @method editRole
         * @method post
         * @return object
         * @throws exception
         * 
         */
        public function editRole() : void
        {
            try
            {
                #Receive params
                (object) $post = $this->request->inputJson(true);
                
                #List roles
                (string) $editResponse = $this->di->AdminModel->editRole($post);
                
                #Response body
                $this->request->response(200, [Constant::MESSAGE => $deleteResponse]);
            }
            catch (Exception $ex)
            {
                $this->exceptionHandler($ex);
            }
        }


        /**
         * @method deleteRole
         * @method post
         * @return object
         * @throws exception
         */
        public function deleteRole() : void
        {
            try
            {
                #Receive params
                (object) $post = $this->request->inputJson(true);
                
                #Delete Role
                (string) $deleteResponse = $this->di->AdminModel->deleteRole($post->id);
                
                #Body response
                $this->request->response(200, [Constant::MESSAGE => $deleteResponse]);
            }
            catch (Exception $ex)
            {
                $this->exceptionHandler($ex);
            }
        }
    }    

