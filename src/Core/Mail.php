<?php
    namespace Core;
    use Core\Mail\Smtp;
    use Core\Mail\MailProtocol;
    use Core\Mail\Templates\DefaultTemplate;
    use Core\Languages\Language;


    /**
    * @copyright (c) Nerdtrix LLC 2021
    * @author Name: Jerry Urena
    * @author Social links:  @jerryurenaa
    * @author email: jerryurenaa@gmail.com
    * @author website: jerryurenaa.com
    * @license MIT (included with this project)
    */


    class Mail
    {         

        /**
         * @method sendMail
         * @param string subject, to, name, body
         */
        protected static function sendMail(string $subject, string $to, string $name, string $body) : void
        {
            #While in production we use the official PHP mail class
            if(PRODUCTION)
            {
                $smtp =  new MailProtocol();
            }

            #While development we use the native SMTP protocol
            if(!PRODUCTION)
            {
                $smtp =  new Smtp();
            }
            
            $smtp->subject = $subject;
            $smtp->body = $body;
            $smtp->to = [$name => $to];

            $confirm = $smtp->send();

            //print_r($confirm);
        }


        /**
         * @method resetPsw
         * @param string name, email
         * Sends a password change confirmation. 
         */
        public static function resetPsw(string $name, string $email) : void
        {
            $lang = new Language(); 
            $template = new DefaultTemplate();

            #Title
            $template->title = sprintf("<h3>%s <strong>%s,</strong></h3>", $lang->translate("hello"), $name);

            #Preheader
            $template->preHeader = $lang->translate("your_password_has_been_updated");

            #Body
            $template->body = sprintf("<p>%s</p><br>", $lang->translate("your_password_has_been_updated"));
            $template->body .= sprintf("<br><p>%s</p>", $lang->translate("if_you_did_not_make_this_change"));

            #Build the template
            $template->build();
            
            #Send mail
            self::sendMail($lang->translate("password_reset_confirmation"), $email, $name, $template->content);

        }

         /**
         * @method sendToken
         * @param string name, email, token
         * sends a new token email to the requested user
         */
        public static function sendOtpToken(string $name, string $email, string $token) : void
        {
            $lang = new Language(); 
            $template = new DefaultTemplate();

            #Title
            $template->title = sprintf("<h3>%s <strong>%s,</strong></h3>", $lang->translate("hello"), $name);

            #Body
            $template->body = sprintf("<p>%s</p><br>", $lang->translate("below_is_your_code"));
            $template->body .= sprintf("<h2><b>%s</b></h2>", $token);
            $template->body .= sprintf("<br><p>%s</p>", $lang->translate("if_you_are_having_trouble"));

            #Build the template
            $template->build();
            
            #Send mail
            self::sendMail($lang->translate("verification_code"), $email, $name, $template->content);
        }



        /**
         * @method sendToken
         * @param string name, email, token
         * sends a new token email to the requested user
         */
        public static function sendToken(string $name, string $email, string $token) : void
        {
            $lang = new Language(); 
            $template = new DefaultTemplate();

            #Title
            $template->title = sprintf("<h3>%s <strong>%s,</strong></h3>", $lang->translate("hello"), $name);

            #Preheader
            $template->preHeader = $lang->translate("we_have_received_your_request");

            #Body
            $template->body = sprintf("<p>%s</p><br>", $lang->translate("we_have_received_your_request"));
            $template->body .= sprintf("<h2>%s: <b>%s</b></h2>", $lang->translate("your_code_is"), $token);
            $template->body .= sprintf("<br><p>%s</p>", $lang->translate("if_you_did_not_request_this_code"));

            #Build the template
            $template->build();
            
            #Send mail
            self::sendMail($lang->translate("password_reset_token"), $email, $name, $template->content);
        }

         /**
         * @method sendWelcomeEmail
         * @param string name, email
         * sends a new token email to the requested user
         */
        public static function sendWelcomeEmail(string $name, string $email) : void
        {
            $lang = new Language(); 
            $template = new DefaultTemplate();

            #Title
            $template->title = sprintf("<h3>%s <strong>%s,</strong></h3>", $lang->translate("hello"), $name);

            #Preheader
            $template->preHeader = $lang->translate("your_account_has_been_created");

            #Body
            $template->body = sprintf("<p>%s</p>", $lang->translate("your_account_has_been_created"));

            #Build the template
            $template->build();

            $subject = sprintf("%s %s", $lang->translate("welcome_to"), EZENV["DEFAULT_APP_NAME"]);
            
            #Send mail
            self::sendMail($subject, $email, $name, $template->content);
        }


        /**
         * @method newDeviceNotification
         * @param string name, email, userIp, userAgent
         * sends a notification email.
         */
        public static function newDeviceNotification(
            string $name, 
            string $email, 
            string  $userIp, 
            string $userAgent) : void
        {
            $lang = new Language(); 
            $template = new DefaultTemplate();

            #Title
            $template->title = sprintf("<h3>%s <strong>%s,</strong></h3>", $lang->translate("hello"), $name);

            #Preheader
            $template->preHeader = $lang->translate("we_detected_a_new_device");

            #Body
            $template->body = sprintf("<p>%s</p><br>", $lang->translate("we_detected_a_new_device"));
            $template->body .= sprintf("<p>%s:</p>", $lang->translate("a_new_login_was_attempted"));
            $template->body .= sprintf("<p>%s: %s</p>", "ip", $userIp);
            $template->body .= sprintf("<p>%s: %s</p>", "navegador", $userAgent);

            $template->body .= sprintf("<br><p>%s</p>", $lang->translate("if_you_did_not_make_this_change"));

            #Build the template
            $template->build();
            
            #Send mail
            self::sendMail($lang->translate("login_from_a_new_device_detected"), $email, $name, $template->content);
        }
    }