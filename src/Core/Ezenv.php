<?php
    namespace Core;
    use \SplFileObject;


    /**
    * @copyright (c) Nerdtrix LLC 2021
    * @author Name: Jerry Urena
    * @author Social links:  @jerryurenaa
    * @author email: jerryurenaa@gmail.com
    * @author website: jerryurenaa.com
    * @license MIT (included with this project)
    * @comment Ezenv class
    */

    class Ezenv
    {
        /**
         * @method load
         * this method will set the global enviroment variables from 
         * .env.development and .env.production. 
         */
        public static function load() : void
        {
            $env = [];

            $envFile = new SplFileObject(
                sprintf("%s%s%s",
                    ROOT_DIR,
                    SLASH,
                    PRODUCTION ? ".env.production" : ".env.development"
                )
            );

            //Build the array with each line
            while (!$envFile->eof())
            {
                $splitEnv = explode("=", $envFile->fgets());

                if(!empty($splitEnv[0]))
                {
                    #is string
                    if(strpos($splitEnv[1], '"'))
                    {
                        $env[trim($splitEnv[0])] = trim(str_replace('"', "", $splitEnv[1]));
                    }
                    else
                    {
                        #int or boolean
                        $env[trim($splitEnv[0])] = trim($splitEnv[1]);
                    }
                }
            }    
    
            $envFile = null; //Unset the file to prevent memory leaks
        
            //Define the global EZENV global variable
            define('EZENV', $env);
        }

        /**
         * TODO
         * Allow comments
         * Allow linebreaks
         */
    }