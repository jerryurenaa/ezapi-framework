<?php
  namespace Core;
  use Core\Languages\Language;
  use Core\DI;
  use Core\Constant;
  use Core\Logger;
  use Core\Helper;
  use \Exception;
 

  /**
    * @copyright (c) Nerdtrix LLC 2021
    * @author Name: Jerry Urena
    * @author Social links:  @jerryurenaa
    * @author email: jerryurenaa@gmail.com
    * @author website: jerryurenaa.com
    * @license MIT (included with this project)
    */

    

  class Router 
  {
    public $request, $lang, $di;

    public function __construct() 
    {
      //Inject Dependencies
      $dependencyInjection = new DI();
      $this->di =  $dependencyInjection->load(get_called_class());

      //instantiate response
      $this->request = new Request();

      //instantiate language 
      $this->lang = new Language();    
    }


    /**
     * @method exceptionHandler
     * @return object 
     * @comment: This is a helper function to prevent code duplication. 
     */
    public function exceptionHandler($exception) : void
    {
      $ex = $exception->getMessage();

      #Unserialize
      if(Helper::isSerialized($ex))
      {
        $ex = unserialize($ex);
      }
      
      #return error 400 plus error message
      $this->request->response(400, [Constant::ERROR => $ex]);
    }


    public function __destruct()
    {
      //Unload dependencies
      $this->di = null;
    }
  }