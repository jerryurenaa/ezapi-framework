<?php
    namespace Core;


    /**
    * @copyright (c) Nerdtrix LLC 2021
    * @author Name: Jerry Urena
    * @author Social links:  @jerryurenaa
    * @author email: jerryurenaa@gmail.com
    * @author website: jerryurenaa.com
    * @license MIT (included with this project)
    * @comment Globals class
    */


    class Helper
    {
        
        /**
         * Check if a string is serialized or not
         * @method is_serialized
         * @param string $string
         */
        public static function isSerialized(string $string) : bool
        {
            return (@unserialize($string) !== false);
        }

        /**
         * Cleans user input with htmlentities
         * @method sanitize
         * @param  string method
         * @param data 
         * @return string   
         */
        public static function sanitize(string $method, $data) 
        {
            if($method == 'array')
            {
               return filter_var_array($data);
            }
            else if($method == 'arrayPost')
            {
                return filter_input_array(INPUT_POST, $data, FILTER_SANITIZE_STRING);
            }
            else if($method == 'arrayGet')
            {
                return filter_input_array(INPUT_GET, $data, FILTER_SANITIZE_STRING);
            }
            else if($method == 'database')
            {
                return htmlspecialchars(strip_tags($data));
            }
            else if($method == 'regular')
            {
                return htmlentities($data, ENT_QUOTES, 'UTF-8');
            }
            else
            {
                return "Invalid method: {$method}";
            }
            
        }

        public static function sanitizeObject(object $objectData) : object
        {
            foreach ($objectData as $value) 
            {
                if (is_scalar($value)) 
                {
                    $value = filter_var($value, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);

                    continue;
                }
        
                sanitize($value);
            }
        
            return $objectData;
        }


        /**
         * @method getClientIP
         * @return string client ip
         * 
         * we use multiple ways to get client IP because remote_addr 
         * may not actually give the real ip since it will give the 
         * proxy ip if the user is using a proxy.
         * 
         * to do
         * later we shuld implement a better way to identify proxies since 
         * this method might lead to security risks because headers can be set by anyone
         */
        public static function getClientIP() : string 
        {
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
            {
                return $_SERVER['HTTP_CLIENT_IP'];
            } 
            elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) 
            {
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
            } 
            else 
            {
                return $_SERVER['REMOTE_ADDR'];
            }
        }


        /**
         * @method getUserAgent
         * @return string currrent user agent
         * 
         * note: this will get current agent info.
         */
        public static function getUserAgent() : string
        {
            return preg_replace('/\/[a-zA-Z0-9.]+/', '', $_SERVER['HTTP_USER_AGENT']);
        }

        

        /**
         * @method isJSON
         * @param array | json string
         * @return boolean
         */
        public static function isJSON(string $checkObject) : bool
        {
            #decode the JSON data
            $result = json_decode($checkObject);

            if (json_last_error() === JSON_ERROR_NONE) 
            {
               return true;
            }

            if (json_last_error() === 0) 
            {
                return true;
            }

            return false;
        }


        /**
         * @method rangeDate
         * @param date date
         * @return int
         * @comment Find the difference in days from 
         * the the previews date passed as a paramerter.
         * Example 0 is today 1 is yesterday 2 is the day before yesterday etc.
         */
        public static function rangeDate($date) : int
        {
            return floor((strtotime(TIMESTAMP) - strtotime($date)) / (60 * 60 * 24));
        }

    }