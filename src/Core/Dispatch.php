<?php
  namespace Core;
  use Core\Error;
  use Core\Config;
  use Core\Router;
  use Core\Request;
  use Core\Ezenv;
  use Core\Session;
  use Core\RBAC;
  use Core\Constant;
  use Core\Globals;
  use \Exception;


  /**
  * @copyright (c) Nerdtrix LLC 2021
  * @author Name: Jerry Urena
  * @author Social links:  @jerryurenaa
  * @author email: jerryurenaa@gmail.com
  * @author website: jerryurenaa.com
  * @license MIT (included with this project)
  * @comment Dispatch class
  */


  /**
   * Warning: any change you make here will affect the whole application.
   * Be sure to check the documentation before modifing anything in this class.
   * If you find any bugs please report them ASAP
   */
  class Dispatch
  { 
    /**
     * @method request
     * This is the entry point where we determine and handle all the request
     */
    public static function request()
    {

      //Load global configuration
      Config::load();

      /**
       * Enviroment variables should be loaded 
       * right after the request class to prevent
       * future issues.
       */
      Ezenv::load();

      #request Helper
      $api = new Request();

      
      /**
       * 
       * To prevent cors issues while debugging or in localhost The first thing we include 
       * to the request is the header settings. You can change these settings to your liking
       * in core/request. 
       */
      $api->header();

      
      /**
       * When a request is made we get the path request and convert it to  an array.
       */
      $pathInfo = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : $_SERVER['ORIG_PATH_INFO'];

      $request = preg_split("#/#", ltrim($pathInfo, "/"));
      

      /**
       * After a request is made we will get the index of the array if it is not empty
       * and try to find a class with this name. If the index is empty we will then use the 
       * default route defined in the config file. 
       * please note that Route is a folder in src and the backslash cannot be changed for any reason
       * this is case sensitive because we are autoloading the classes.
       */
      $route =  sprintf("Routes\%s", ucwords(empty($request[0]) && empty($request[1]) ? DEFAULT_ROUTE : $request[0]));


      /**
       * If the 2nd object in the array $request is not empty we will use that as default method.
       * If the 2nd object in the array is empty we will get the index object otherwise if both
       * index and 2nd objects are empty we will by default use index.
       */    
      $method = !empty($request[0]) && !empty($request[1]) ? $request[1] : 'index';


      /**
       * Secure the connection with a valid SSL certificate on production.
       */
      if(PRODUCTION && EZENV["ENFORCE_SSL"])
      {
        if (empty($_SERVER["HTTPS"]) || $_SERVER["SERVER_PORT"] != "443") 
        {
          $api->response(495,  [Constant::ERROR => Constant::SSL_REQUIED]);
        }
      }


      /*
      * If the class is not found or if the method does not exist in that class
      * We will return error code 404.
      */
      if(!class_exists($route) || !method_exists($route, $method))
      {
        $api->response(404,  [Constant::ERROR => Constant::INVALID_REQUEST]);
      }

      /*
      * Before instantiating the requested method we first validate the user with the authentication 
      * system or verify that the requested method is in the allow  array.
      */
      if(isset($_COOKIE[USER_SESSION_NAME]) && !Session::extend() ||
        !isset($_COOKIE[USER_SESSION_NAME]) && !RBAC::isViewAllow(Globals::$userRole, $route, $method))
      {
        $api->response(401, [Constant::ERROR => Constant::AUTH_REQUIRED]);
      } 

      /**
       * Verify that the current logged in user has access
       * to the requested route.
       */
      if(!RBAC::isViewAllow(Globals::$userRole, $route, $method))
      {
        $api->response(403, [Constant::ERROR => Constant::ACCESS_DENIED]);
      }

      /**
       * Because Everything will come In and out and beign handle by this class we will catch any error
       * and filter them with our error handler class.
       */
      try
      {
        /**
       * If all validations are passed We will pass the params to the method requested
       * and trigger the method as a new instance.
       */
        $run = new $route();
        $run->$method();
      }
      catch(Exception $ex)
      {
        Error::handler($ex);
      }
    }
  }