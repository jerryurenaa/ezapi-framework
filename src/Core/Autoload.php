<?php
    namespace Core;
    use Core\Constant;

    
    /**
    * @copyright (c) Nerdtrix LLC 2021
    * @author Name: Jerry Urena
    * @author Social links:  @jerryurenaa
    * @author email: jerryurenaa@gmail.com
    * @author website: jerryurenaa.com
    * @license MIT (included with this project)
    * @comment All folders, file name and classes within the src directory
    * must start with a capital letter to prevent autoloading conflicts.
    */

    
    #Autoload
    spl_autoload_register(function ($className)
    {
        $fileName = sprintf(
            "%s%s%s.php", 
            dirname(__DIR__),  
            DIRECTORY_SEPARATOR, 
            str_replace("\\", "/", 
            $className
        ));

        if (file_exists($fileName))
        {
            require ($fileName);
        }
        else
        {
            die(sprintf("%s: %s", Constant::CLASS_NOT_FOUND, $fileName));
        }
    });