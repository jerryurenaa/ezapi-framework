<?php
    namespace Core\Languages;
    use Core\Languages\Locale;
    use Core\Languages\Dictionary;
    use Core\Cookie;
    use Core\Constant;
    use Core\DI;    


    /**
    * @copyright (c) Nerdtrix LLC 2021
    * @author Name: Jerry Urena
    * @author Social links:  @jerryurenaa
    * @author email: jerryurenaa@gmail.com
    * @author website: jerryurenaa.com
    * @license MIT (included with this project)
    */

    
    class Language
    {
        #Local variables
        private 
            $_locale, 
            $di;


        public function __construct()
        {
            #Dependency injection
            $dependencyInjection = new DI();
            $this->di = $dependencyInjection->load(get_called_class());

            #Set locale
            if(Cookie::exists("locale"))
            {
                $this->_locale = Cookie::get("locale");
            }
            else
            {
                $this->setLocale(EZENV["DEFAULT_LANGUAGE_LOCALE"]);
            }
        }  
        

        /**
         * @method list
         * @return array
         * list supported language name, locale and chartset
         */
        public function list() : array
        {
            (array) $list = [];

            (array) $locales = $this->di->Dictionary->supportedLocales();

            foreach($locales as $locale)
            {
                #Get list
                (array) $languageList = $this->di->Locale->get($locale);

                #Add locale
                $languageList["locale"] = $locale;

                #Add to the list
                $list[] = $languageList;
            }

            return $list;
        }


        /**
         * @method currentLocale
         * @return string
         */
        public function currentLocale() : string
        {
            if(Cookie::exists("locale"))
            {
                return Cookie::get("locale");
            }

            return Constant::LOCALE_VALUE_NOT_SET;
        }


        /**
         * @method setLocale
         * @param string locale
         * @return string
        */
        public function setLocale(string $locale) : string
        {
            #Validate locale
            if(!in_array($locale, $this->di->Dictionary->supportedLocales()))
            {
                return Constant::INVALID_LOCALE;
            } 

            #Store preference in a cookie for one year
            if(Cookie::set("locale", $locale, CURRENT_TIME + 31556926))
            {
                #set locale value
                $this->_locale = $locale; 

                return Constant::SUCCESS;
            }

            return Constant::UNABLE_TO_SET_COOKIE;
        }


        /**
         * @method translate
         * @param string key
         * @return string
        */
        public function translate(string $key) : string
        {
            return $this->di->Dictionary->get($key, $this->_locale);
        }


        /**
         * @method translateArray
         * @param array keys
         * @return array
         */
        public function translateArray(array $keys) : array
        {
            #If the key is an array only supports 1 level nested
            if(is_array($keys))
            {
                $buildArr = [];

                foreach($keys as $index => $k)
                {
                    $buildArr[$index] = $this->di->Dictionary->get($k, $this->_locale);  
                }

                return $buildArr;
            }

            return [Constant::ERROR => Constant::YOU_MUST_PROVIDE_A_VALID_ARRAY];
        }

        
        #Destructor
        public function __destruct()
        {
            //Unload dependencies
            $this->di = null;
        }
    }