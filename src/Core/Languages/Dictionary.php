<?php
    namespace Core\Languages;
    use Core\Constant;


    /**
    * @copyright (c) Nerdtrix LLC 2021
    * @author Name: Jerry Urena
    * @author Social links:  @jerryurenaa
    * @author email: jerryurenaa@gmail.com
    * @author website: jerryurenaa.com
    * @license MIT (included with this project)
    */


    class Dictionary 
    {

        /**
         * Dictionary list. 
         */
        private $keyList = [

            "hello_world" => [
                "en_US" => "Hello world",
                "es_US" => "Hola mundo"
                #Add any other language here
            ],
            "welcome" => [
                "en_US" => "Welcome",
                "es_US" => "Bienvenido"
            ],
            "welcome_to" => [
                "en_US" => "Welcome to",
                "es_US" => "Bienvenido a"
            ],
            "invalid_username_or_email" => [
                "en_US" => "Invalid username or email",
                "es_US" => "Nombre de usuario o correo invalido"
            ],
            "username_or_email_is_required" => [
                "en_US" => "username or email is required",
                "es_US" => ""
            ],
            "invalid_password" => [
                "en_US" => "Invalid password",
                "es_US" => "Contraseña invalida"
            ],
            "current_password_password" => [
                "en_US" => "Current password is required",
                "es_US" => "Contraseña invalida"
            ],
            "current_password_is_invalid" => [
                "en_US" => "Current password is invalid",
                "es_US" => "Contraseña invalida"
            ],
            "first_name_is_required" => [
                "en_US" => "First name is required",
                "es_US" => ""
            ],
            "last_name_is_required" => [
                "en_US" => "Last name is required",
                "es_US" => ""
            ],
            "username_is_required" => [
                "en_US" => "Username is required",
                "es_US" => ""
            ],
            "email_is_required" => [
                "en_US" => "Email is required",
                "es_US" => ""
            ],
            "password_is_required" => [
                "en_US" => "Password is required",
                "es_US" => ""
            ],
            "password_confirmation_is_required" => [
                "en_US" => "Password confirmation is required",
                "es_US" => ""
            ],
            "invalid_email_address" => [
                "en_US" => "Invalid email address",
                "es_US" => ""
            ],
            "this_email_already_exists" => [
                "en_US" => "This email already exists",
                "es_US" => ""
            ],
            "your_username_is_too_short" => [
                "en_US" => "Your username is too short",
                "es_US" => ""
            ],
            "your_username_is_too_long" => [
                "en_US" => "Your username is too long",
                "es_US" => ""
            ],
            "this_username_already_exists" => [
                "en_US" => "This username already exists",
                "es_US" => ""
            ],
            "your_password_is_too_short" => [
                "en_US" => "Password is too short",
                "es_US" => ""
            ],
            "your_password_is_too_long" => [
                "en_US" => "Password is too long",
                "es_US" => ""
            ],
            "your_password_does_not_match" => [
                "en_US" => "Your passwords do not match",
                "es_US" => ""
            ],
            "your_password_confirmation_doesnt_match" => [
                "en_US" => "Your password confirmation does not match",
                "es_US" => ""
            ],
            "hello" => [
                "en_US" => "Hello",
                "es_US" => ""
            ],
            "we_have_received_your_request" => [
                "en_US" => "We received a request to change your password.",
                "es_US" => ""
            ],
            "below_is_your_code" => [
                "en_US" => "Below is your one-time verification code",
                "es_US" => ""
            ],
            "if_you_did_not_request_this_code" => [
                "en_US" => "If you did not request this code, it is possible that someone else is trying to access your account. Please do not share this code with anyone.",
                "es_US" => ""
            ],
            "password_reset_token" => [
                "en_US" => "Password reset token",
                "es_US" => ""
            ],
            "your_account_has_been_created" => [
                "en_US" => "Your account has been created successfully.",
                "es_US" => ""
            ],
            "your_password_has_been_updated" => [
                "en_US" => "Your password has been updated successfully.",
                "es_US" => ""
            ],
            "if_you_did_not_make_this_change" => [
                "en_US" => "If you did not make this change please contact our tech support immediately.",
                "es_US" => ""
            ],
            "password_reset_confirmation" => [
                "en_US" => "Password reset confirmation",
                "es_US" => ""
            ],
            "we_detected_a_new_device" => [
                "en_US" => "We detected a new login with your credentials from a new device or new browser.",
                "es_US" => ""
            ],
            "a_new_login_was_attempted" => [
                "en_US" => "a new login was attempted from the following",
                "es_US" => ""
            ],
            "login_from_a_new_device_detected" => [
                "en_US" => "Login from new device detected",
                "es_US" => ""
            ],
            "if_you_are_having_trouble" => [
                "en_US" => "If you are having any issues with your account, please don't hesitate to contact us.",
                "es_US" => ""
            ],
            "verification_code" => [
                "en_US" => "One time password",
                "es_US" => "Token de verificación"
            ],
            "invalid_token" => [
                "en_US" => "Invalid token",
                "es_US" => ""
            ],
            "registrations_are_currently_disabled" => [
                "en_US" => "Registrations are currently disabled",
                "es_US" => ""
            ],
            "please_logout_and_try_again" => [
                "en_US" => "Please logout and try again",
                "es_US" => ""
            ],
            "OTP_SENT" => [
                "en_US" => "A one-time verification code has been sent to your email",
                "es_US" => ""
            ],
            "this_account_is_already_activated" => [
                "en_US" => "This account is already activated",
                "es_US" => ""
            ],
            "account_banned" => [
                "en_US" => "This account is banned",
                "es_US" => ""
            ],
            "access_to_this_field_is_forbidden" => [
                "en_US" => "You do not have access to update this field",
                "es_US" => ""
            ],
            "invalid_language_locale" => [
                "en_US" => "Invalid language locale",
                "es_US" => ""
            ],
            "please_specify_a_keyword" => [
                "en_US" => "please specify a keyword",
                "es_US" => "por favor especifique una palabra clave"
            ],
            "there_are_no_results" => [
                "en_US" => "There are no results that match your search",
                "es_US" => "No hay resultados que coincidan con su búsqueda"
            ],
            "user_id_required" => [
                "en_US" => "A user id is required",
                "es_US" => "Id de usuario requerido"
            ],
            "you_cannot_delete_yourself" => [
                "en_US" => "You cannot delete yourself",
                "es_US" => ""
            ],
            "invalid_user_id" => [
                "en_US" => "Invalid user id",
                "es_US" => ""
            ],
            "invalid_status" => [
                "en_US" => "Invalid status",
                "es_US" => ""
            ],
            "invalid_role" => [
                "en_US" => "Invalid role",
                "es_US" => ""
            ],
            "account_inactive" => [
                "en_US" => "This account is inactive",
                "es_US" => ""
            ],
            "a_route_is_required" => [
                "en_US" => "A route is required",
                "es_US" => ""
            ],
            "at_least_one_action_is_required" => [
                "en_US" => "At least one action is required",
                "es_US" => ""
            ],
            "a_role_is_required" => [
                "en_US" => "A role is required",
                "es_US" => ""
            ],
            "at_least_one_permission_must_be_set" => [
                "en_US" => "At least one permission must be set",
                "es_US" => ""
            ],
            "you_do_not_have_permissions" => [
                "en_US" => "You to nor have permissions to perform this action",
                "es_US" => ""
            ]
            

            
            
            


            
           


            

        ];


        /**
         * @method get
         * @param string key
         * @param string locale
         * @return string         * 
         */
        public function get(string $key, string $locale) : string
        {
            if(array_key_exists($key, $this->keyList))
            {
                if(array_key_exists($locale, $this->keyList[$key]))
                {
                    return $this->keyList[$key][$locale];
                }
            }

            return Constant::INVALID_KEY_OR_LOCALE;
        }


        /**
         * @method supportedLocales
         * @return array
         * @comment This method returns the current supported locales
         */
        public function supportedLocales() : array
        {
            $locales = [];

            #use first key to list locales
           foreach($this->keyList[array_key_first($this->keyList)] as $key => $unusedVal)
           {
                $locales[] = $key; 
           }

           return $locales;
        }
    }