<?php
    namespace Core;

     /**
    * @copyright (c) Nerdtrix LLC 2021
    * @author Name: Jerry Urena
    * @author Social links:  @jerryurenaa
    * @author email: jerryurenaa@gmail.com
    * @author website: jerryurenaa.com
    * @license MIT (included with this project)
    * @comment Crypto static class
    */


    class Crypto
    {        
        /**
         * @method randomHash
         * @return string 
         */
        public static function randomHash() : string
        {
            return md5(bin2hex(random_bytes(16)));
        }


        /**
         * @method randomNumber
         * @param int length
         * @return string 
         */
        public static function randomNumber(int $length) : string
        {
            $result = null;

            for($i = 0; $i < (int) $length; $i++) 
            {
                $result .= mt_rand(0, 9);
            }

            return $result;
        }


        /**
         * @method randomToken
         * @return string 
         */
        public static function randomToken() : string
        {
            return base64_encode(openssl_random_pseudo_bytes(32));
        }
    }