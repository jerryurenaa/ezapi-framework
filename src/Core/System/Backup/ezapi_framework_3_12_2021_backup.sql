-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 12, 2021 at 07:54 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ezapi_framework`
--

-- --------------------------------------------------------

--
-- Table structure for table `rbac`
--

CREATE TABLE `rbac` (
  `id` int(11) NOT NULL,
  `route` varchar(100) NOT NULL,
  `action` text NOT NULL,
  `role` varchar(50) NOT NULL,
  `permission` set('VIEW','ADD','EDIT','DELETE') NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rbac`
--

INSERT INTO `rbac` (`id`, `route`, `action`, `role`, `permission`, `description`) VALUES
(1, 'User', 'index, login, register, activateAccount, setLanguageLocale, listLanguages, resetPsw', 'GUEST', 'VIEW,ADD', ''),
(2, 'User', '*', 'USER', 'VIEW,ADD,EDIT,DELETE', ''),
(3, 'User', '*', 'ADMIN', 'VIEW,ADD,EDIT,DELETE', ''),
(4, 'Admin', '*', 'ADMIN', 'VIEW,ADD,EDIT,DELETE', ''),
(5, 'Install', '*', 'GUEST', 'VIEW,ADD', '');

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `session` text NOT NULL,
  `agentName` varchar(300) NOT NULL,
  `agentIdentifier` text NOT NULL,
  `ip` varchar(100) NOT NULL,
  `time` datetime NOT NULL,
  `expire` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`id`, `userId`, `session`, `agentName`, `agentIdentifier`, `ip`, `time`, `expire`) VALUES
(59, 45, '', 'PostmanRuntime', '553f0e68e8e782666a828d0b5ef3da11', '::1', '2021-02-24 13:22:18', '2021-02-24 13:09:49'),
(65, 45, 'f354899007542dccd154d49b46d26b10', 'Mozilla (Windows NT 10.0; Win64; x64) AppleWebKit (KHTML, like Gecko) Chrome Safari', '439c3a66576f50447b08eea6081b9e12', '::1', '2021-02-27 17:09:53', '2021-02-27 17:45:21'),
(66, 45, '661826c8925bef831fdb9804a908ec03', 'Mozilla (Windows NT 10.0; Win64; x64) AppleWebKit (KHTML, like Gecko) Chrome Safari', '8780ef3bf0a99f5dee4588a5ca5adc8b', '::1', '2021-03-01 22:21:49', '2021-03-01 23:02:14');

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `token` text NOT NULL,
  `expiration` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `token`
--

INSERT INTO `token` (`id`, `userId`, `token`, `expiration`) VALUES
(109, 45, '091284', '2021-03-02 16:28:04');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `fName` varchar(100) NOT NULL,
  `lName` varchar(100) NOT NULL,
  `twoFactorAuth` tinyint(4) NOT NULL,
  `role` varchar(50) NOT NULL,
  `status` enum('inactive','active','banned') NOT NULL,
  `locale` varchar(20) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`, `fName`, `lName`, `twoFactorAuth`, `role`, `status`, `locale`, `created`) VALUES
(44, 'mastersoft15@hotmail.com', 'mastersoft15@gmail.com', '$2y$10$.JC302AjiUXiexWJCrUqJuMl.tAhuwukYrbPjoa8blohKnQwjytqG', 'jorge', 'miguel', 0, 'USER', 'banned', 'es_US', '2021-02-12 17:22:32'),
(45, 'mastersoft15', 'mastersoft15@hotmail.com', '$2y$10$.JC302AjiUXiexWJCrUqJuMl.tAhuwukYrbPjoa8blohKnQwjytqG', 'jorge', 'miguel', 1, 'ADMIN', 'active', 'en_US', '2021-02-12 17:22:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rbac`
--
ALTER TABLE `rbac`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rbac`
--
ALTER TABLE `rbac`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `session`
--
ALTER TABLE `session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `token`
--
ALTER TABLE `token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
