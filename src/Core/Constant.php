<?php
    namespace Core;


    /**
    * @copyright (c) Nerdtrix LLC 2021
    * @author Name: Jerry Urena
    * @author Social links:  @jerryurenaa
    * @author email: jerryurenaa@gmail.com
    * @author website: jerryurenaa.com
    * @license MIT (included with this project)
    * @comment Global constants class. We use constants to prevent 
    * spelling mistakes and to make it easier to translate our strings.
    */


    class Constant
    {
        #Language constants
        const INVALID_KEY_OR_LOCALE = "invalid key or locale";
        const INVALID_LOCALE = "Invalid locale";
        const UNABLE_TO_SET_COOKIE = "Unable to set locale cookie";
        const LOCALE_VALUE_NOT_SET = "Locale value not set";
        const YOU_MUST_PROVIDE_A_VALID_ARRAY = "You must provide a valid array";

        #Autoload constants
        const CLASS_NOT_FOUND = "Class not found: ";

        #Global constants
        const SUCCESS = "success";
        const ERROR = "error";
        const MESSAGE = "message";
        const OTP_REQUIRED = "OTP required";
        const ACTION = "action";
        const SUCCESS_MESSAGE = "Operation successful";
        const OTP_SENT = "Token sent";
        const ERROR_MESSAGE = "Oops, something went wrong";
        const INVALID_REQUEST = "Invalid request";
        const ACTION_FORBIDDEN = "Action forbidden";
        const DETAILS = "details";
        const SSL_REQUIED = "A valid SSL Certificate is required";
        const AUTH_REQUIRED = "Authentication required";
        const ACCESS_DENIED = "Access Denied";
        const INVALID_JSON = "Invalid Json";


      
        
        
        





        #these messages can be safely translated
        
       
        
        
        const FIELDS_REQUIRED = "All fields are required";
        
        
        
        const INVALID_DICTIONARY = "Dictionary does not exist!";
        const UNABLE_TO_FIND_PATH = "Unable to find filepath";
        
        const INVALID_AGENT_TOOL = "Invalid agent tool";
        const BANNED_IP_ADDRESS = "Banned Ip address";
        const MULTI_LOGIN_NOT_ALLOWED = "You cannot login with multiple devices at the same time please logout from the other one and try again";
        const MAXIMUN_LOGIN_COUNT_REACHED = "you have reached the maximun login count please logout from some devices and try again";
        const UNABLE_TO_SET_SESSION = "Unable to set session";
        
        const INVALID_DATA_RECEIVED = "Invalid Data received";

    }