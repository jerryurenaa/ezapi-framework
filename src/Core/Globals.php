<?php
    namespace Core;


    /**
    * @copyright (c) Nerdtrix LLC 2021
    * @author Name: Jerry Urena
    * @author Social links:  @jerryurenaa
    * @author email: jerryurenaa@gmail.com
    * @author website: jerryurenaa.com
    * @license MIT (included with this project)
    * @comment Globals class
    */

    /**
     * GLobal variables: 
     * Any variable declared here can be accesded from anywhere within the app.
     */
    class Globals
    {
        public static $userId = null;
        public static $userRole = "GUEST";
        public static $userLanguage = null;
    }