<?php
    namespace Core\Mail;

class Dictionary
{
    private $headerType = [];
    
    private $contentType = [ 
        'txt' => 'text/plain',
        'htm' => 'text/html',
        'html' => 'text/html',
        'php' => 'text/html',
        'css' => 'text/css',
        'js' => 'application/javascript',
        'json' => 'application/json',
        'xml' => 'application/xml',
        'swf' => 'application/x-shockwave-flash',
        'flv' => 'video/x-flv',
        'png' => 'image/png',
        'jpe' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpg' => 'image/jpeg',
        'gif' => 'image/gif',
        'bmp' => 'image/bmp',
        'ico' => 'image/vnd.microsoft.icon',
        'tiff' => 'image/tiff',
        'tif' => 'image/tiff',
        'svg' => 'image/svg+xml',
        'svgz' => 'image/svg+xml',
        'zip' => 'application/zip',
        'rar' => 'application/x-rar-compressed',
        'exe' => 'application/x-msdownload',
        'msi' => 'application/x-msdownload',
        'cab' => 'application/vnd.ms-cab-compressed',
        'mp3' => 'audio/mpeg',
        'qt' => 'video/quicktime',
        'mov' => 'video/quicktime',
        'pdf' => 'application/pdf',
        'psd' => 'image/vnd.adobe.photoshop',
        'ai' => 'application/postscript',
        'eps' => 'application/postscript',
        'ps' => 'application/postscript',
        'doc' => 'application/msword',
        'rtf' => 'application/rtf',
        'xls' => 'application/vnd.ms-excel',
        'ppt' => 'application/vnd.ms-powerpoint',
        'docx' => 'application/msword',
        'xlsx' => 'application/vnd.ms-excel',
        'pptx' => 'application/vnd.ms-powerpoint',
        'odt' => 'application/vnd.oasis.opendocument.text',
        'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
    ];

    private  $CODE = [
        101	=> "Connection error (invalid host or port)",
        211	=> "System status (response to HELP)",
        214	=> "Help message (response to HELP)",
        220	=> "MAIL Service ready", 
        221	=> "Service closing transmission channel",
        235	=> "Authentication successful",
        250	=> "Operation successful", 
        251	=> "Not a local user but the server will forward the message",
        252	=> "The server cannot verify the user",
        334	=> "Encrypted connection successful",
        354	=> "Ready to send email",
        421	=> "The server is not unavailable",
        422	=> "The recipient’s mailbox has exceeded its storage limit",
        431	=> "File overload (too many messages sent to a particular domain)",
        441	=> "No response from the recipient’s server",
        442	=> "Connection dropped",
        446	=> "Internal loop has occurred",
        450	=> "Mailbox unavailable",
        451	=> "The server aborted the command due to a local error",
        452	=> "The server aborted the command due to insufficient system storage",
        454	=> "TLS temporarily not available",
        455	=> "Parameters cannot be accommodated",
        471	=> "Mail server error due to the local spam filter",
        500	=> "Unrecognized command",
        501	=> "Syntax error",
        502	=> "Command not implemented",
        503	=> "Bad sequence of commands",
        504	=> "The server has not implemented a command parameter",
        510	=> "Invalid email address",
        512	=> "DNS error",
        523	=> "Your mailing list exceeds the server limit",
        530	=> "You must run STARTTLS first",
        535	=> "Authentication failed",
        538	=> "Encryption required",
        541	=> "Message rejected by the spam filter",
        550	=> "Mailbox is unavailable",
        551	=> "Not a local User",
        552	=> "The mailbox is full",
        553	=> "Incorrect mail address",
        554	=> "The transaction failed",
        555	=> "Parameters not recognized"
    ];

    #private array
    private $command = [
        "HELO", 
        "EHLO", #Ask the server to use the most recent SMTP version
        "STARTTLS",
        "DATA",
        "AUTH LOGIN",
        "QUIT",
        "MAIL FROM",
        "RCPT TO",
        "NOOP", //Check whether the server can respond with status 250 or not
        "HELP",
        "VRFY",
        "EXPN",
        "RSET",
        "HELP",
        "RSET",
        "AUTH",
        "ATRN",
        "BDAT",
        "ETRN",
        "AUTH OAUTHBEARER",
        "AUTH XOAUTH2"
    ];

    private $tlsVersion = [
        "tls" => STREAM_CRYPTO_METHOD_TLSv1_0_CLIENT,
        "tlsv1.0" => STREAM_CRYPTO_METHOD_TLSv1_0_CLIENT,
        "tlsv1.1" => STREAM_CRYPTO_METHOD_TLSv1_1_CLIENT,
        "tlsv1.2" => STREAM_CRYPTO_METHOD_TLSv1_2_CLIENT
    ];

    public function command(string $send) : string
    {
        return $this->command[$send];
    }

    public function ContentType(string $filePath) : string
    {
        $ext = $this->fileExtension($filePath);

        return $this->$contentType[$ext];
    }


    /**
     * @method fileExtension
     * @param string filePath
     * @return string 
     * Use this method to get the file extension
     */
    public function fileExtension(string $filePath) : string
    {
        #get file extension
        return pathinfo($filePath, PATHINFO_EXTENSION);
    }
 }