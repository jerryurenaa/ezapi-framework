<?php
    namespace Core;

    /**
    * @copyright (c) Nerdtrix LLC 2021
    * @author Name: Jerry Urena
    * @author Social links:  @jerryurenaa
    * @author email: jerryurenaa@gmail.com
    * @author website: jerryurenaa.com
    * @license MIT (included with this project)
    */

    class Logger
    {

        #Logger code statuses
        private static $statusCode = [
            0 => "INFO",
            1 => "WARNING",
            2 => "SUCCESS",
            3 => "ERROR",
            4 => "INVALID STATUS CODE"
        ];

        
        /**
         * @method status
         * @return array
         */
        public static function status() : array
        {
            return self::$statusCode;
        }


        /**
         * @method Write
         * @param string | array $message
         * @param int $status (obtional)
         * @comment write to logger.
         */
        public static function write($message, int $status = 0) : void
        {   
            #Log Start Date
            $log = null;

            #Build caller trace
            $callerPath = debug_backtrace()[1]['class'] . SLASH .  debug_backtrace()[1]['function'];

            #status
            if(!array_key_exists($status, self::$statusCode))
            {
                $status = 4;
            }

            #Append to log string
            if(is_array($message))
            {
                foreach ($message as $key => $val) 
                {
                    $log .= sprintf(
                        "%s: <%s> %s: %s time:%s%s", 
                        $callerPath, 
                        self::$statusCode[$status],  
                        $key, 
                        $val, 
                        TIMESTAMP, 
                        NEWLINE);
                }
            }
            else //string
            {
                $log .= sprintf(
                    "%s: <%s> %s time: %s%s", 
                    $callerPath, 
                    self::$statusCode[$status], 
                    $message, 
                    TIMESTAMP, 
                    NEWLINE);
            }
            
            #Append to logger
            file_put_contents(
                sprintf(
                    "%s%sSystem%sLogger%sLog.txt", 
                    SRC_DIR, 
                    SLASH, 
                    SLASH, 
                    SLASH), 
                $log, 
                FILE_APPEND
            );
        }
    }