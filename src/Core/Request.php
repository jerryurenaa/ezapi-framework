<?php
    namespace Core;
    use Core\Languages\Language;
    use Core\Helper;
    use Core\Constant;
    use \Exception;


    /**
    * @copyright (c) Nerdtrix LLC 2021
    * @author Name: Jerry Urena
    * @author Social links:  @jerryurenaa
    * @author email: jerryurenaa@gmail.com
    * @author website: jerryurenaa.com
    * @license MIT (included with this project)
    */

    

    class Request
    {
        private $_rescodes =  [

            // INFORMATIONAL CODES
            100 => 'Continue',
            101 => 'Switching Protocols',
            102 => 'Processing',

            // SUCCESS CODES
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            207 => 'Multi-status',
            208 => 'Already Reported',

            // REDIRECTION CODES
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => 'Switch Proxy', // Deprecated
            307 => 'Temporary Redirect',

            // CLIENT ERROR
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Time-out',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested range not satisfiable',
            417 => 'Expectation Failed',
            418 => 'I\'m a teapot',
            422 => 'Unprocessable Entity',
            423 => 'Locked',
            424 => 'Failed Dependency',
            425 => 'Unordered Collection',
            426 => 'Upgrade Required',
            428 => 'Precondition Required',
            429 => 'Too Many Requests',
            431 => 'Request Header Fields Too Large',
            495 => 'SSL Certificate Error',

            // SERVER ERROR
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Time-out',
            505 => 'HTTP Version not supported',
            506 => 'Variant Also Negotiates',
            507 => 'Insufficient Storage',
            508 => 'Loop Detected',
            511 => 'Network Authentication Required',
        ];

        private $_allowedMethods = [
            "OPTIONS",
            "POST",
            "GET"
        ];

        private $_allowedHeaders = [
            "Content-Type"
        ];

        private $_allowedContentType = [
            "application/json",
            "application/json; charset=UTF-8"
        ];
  

        /**
         * @method code
         * @param code
         * @return int code 
         */
        public function code($code)
        {
            return $this->_rescodes[$code];
        }


        public function header()
        {
            if (isset($_SERVER["HTTP_ORIGIN"]) && !in_array($_SERVER["HTTP_ORIGIN"], ALLOWED_ORIGINS) && !ALLOW_ANY_API_CONNECTION)
            {
                //error origin not allowed
                $this->response(400, [Constant::ERROR => $this->_rescodes[400]]);
            }

            if (!isset($_SERVER["HTTP_ORIGIN"]))
            {
                //Overwrite in development mode
                if(!PRODUCTION && in_array($_SERVER["REMOTE_ADDR"], ALLOWED_ORIGINS))
                {
                    $_SERVER["HTTP_ORIGIN"] = $_SERVER["REMOTE_ADDR"];
                }
                else
                {
                    $this->response(400, [Constant::ERROR => $this->_rescodes[400]]);
                }
            }

            if(isset($_SERVER["REQUEST_METHOD"]) && !in_array($_SERVER["REQUEST_METHOD"], $this->_allowedMethods))
            { 
                #method not allowed
                $this->response(405, [Constant::ERROR => $this->_rescodes[405]]);
            }

            if(isset($_SERVER["CONTENT_TYPE"]) && !in_array($_SERVER["CONTENT_TYPE"], $this->_allowedContentType))
            {
                
                $this->response(415, [Constant::ERROR => $this->_rescodes[415]]);
            }
            
            #overwrite in development mode
            if (!isset($_SERVER["CONTENT_TYPE"]) && !PRODUCTION)
            {
                $_SERVER["CONTENT_TYPE"] = "application/json; charset=UTF-8";
            }

            if (!isset($_SERVER["CONTENT_TYPE"]) && PRODUCTION)
            {
                $this->response(415, [Constant::ERROR => $this->_rescodes[415]]);
            }
               
            header(sprintf("Access-Control-Allow-Origin: %s", $_SERVER["HTTP_ORIGIN"]));
            header("Access-Control-Allow-Credentials: true"); #Should always be true
            header(sprintf("Access-Control-Allow-Methods: %s", implode(",", $this->_allowedMethods)));
            header(sprintf("Access-Control-Allow-Headers: %s", implode(",", $this->_allowedHeaders)));
            header(sprintf("Content-Type: %s", $_SERVER["CONTENT_TYPE"]));
            header(sprintf("X-Powered-By: %s", EZENV['DEFAULT_APP_NAME']));
            header(sprintf("app-version: %s", EZENV['APP_VERSION']));
            
            if ($_SERVER["REQUEST_METHOD"] === "OPTIONS") 
            {
                exit; // OPTIONS request wants only the policy, we can stop here
            }
        }



        /**
         * @method jsonPost
         * @param bool sanitize (obtional)
         * @return object
         * @throws exception
         */
        public function inputJson(bool $sanitize = false) : object
        {
            #Get params
            $post = file_get_contents("php://input");

            #Validate json
            if(!Helper::isJSON($post))
            {
                throw new Exception(Constant::INVALID_JSON);
            }

            #Convert to object
            $post = json_decode($post);

            if($sanitize)
            {
                $post = Helper::sanitizeObject($post);
            }

            return $post;
        }
        

        public function inputPost(bool $sanitize = false) : object
        {

            // /**
            //  * $_POST && $_GET are discondinue in our framework 
            //  * because multipart/form-data content type is not allowed
            //  * we communicate with our framework using json data.
            //  */
            // // if(isset($_POST) && !empty($_POST) || isset($_GET) && !empty($_GET))
            // // {
            // //     throw new Exception ("Invalid data format");
            // // }
            // $this->response(400, ["s" => "hola"]);
           
            
            // #Receive Json data in POST OR GET
            // $post = file_get_contents("php://input");

           

            // #Only json data are allowed
            // if(Helper::isJSON($post))
            // {
            //     $post = json_decode($post, true);
            // }
            // else
            // {
            //     throw new Exception (Constant::INVALID_DATA_RECEIVED);
            // }

            // if(empty($post))
            // {
            //     throw new Exception (Constant::INVALID_REQUEST);
            // }

            // #Sanitize array
            // if($sanitize)
            // {
            //     $post = Helper::sanitize("array", $post);
            // }

            // return (object) $post;
        }

        public function inputGet(bool $sanitize = false) : object
        {

            // /**
            //  * $_POST && $_GET are discondinue in our framework 
            //  * because multipart/form-data content type is not allowed
            //  * we communicate with our framework using json data.
            //  */
            // // if(isset($_POST) && !empty($_POST) || isset($_GET) && !empty($_GET))
            // // {
            // //     throw new Exception ("Invalid data format");
            // // }
            // $this->response(400, ["s" => "hola"]);
           
            
            // #Receive Json data in POST OR GET
            // $post = file_get_contents("php://input");

           

            // #Only json data are allowed
            // if(Helper::isJSON($post))
            // {
            //     $post = json_decode($post, true);
            // }
            // else
            // {
            //     throw new Exception (Constant::INVALID_DATA_RECEIVED);
            // }

            // if(empty($post))
            // {
            //     throw new Exception (Constant::INVALID_REQUEST);
            // }

            // #Sanitize array
            // if($sanitize)
            // {
            //     $post = Helper::sanitize("array", $post);
            // }

            // return (object) $post;
        }

      


        /**
         * @method response
         * @param int code
         * @param string | array response
         * @return object
         */
        public function response(int $code = 400, array $response) : void
        {
            header("Content-Type: application/json; charset=UTF-8");            
            http_response_code($code);
            
            exit(json_encode($response));
        }
    }