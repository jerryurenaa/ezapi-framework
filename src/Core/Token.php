<?php
    namespace Core;
    use Core\Database;
    use Core\Crypto;
    use Core\Constant;

    /**
    * @copyright (c) Nerdtrix LLC 2021
    * @author Name: Jerry Urena
    * @author Social links:  @jerryurenaa
    * @author email: jerryurenaa@gmail.com
    * @author website: jerryurenaa.com
    * @license MIT (included with this project)
    */
    

    class Token 
    {
           /**
         * @method _db
         * @return instance 
         * @comment: Create a new db instance 
         * using the default table session.
         */
        private static function _db() 
        {
            return new Database(["table" => "token"]);
        }


        public static function get(string $userId, int $expiry = 15, string $type = "numbers")
        {
            $token = null;

            if($type == "numbers")
            {
                $token = Crypto::randomNumber(6);
            }

            $expiration = CURRENT_TIME + ($expiry * 60);

            #Delete previous tokens if there are any
            self::delete($userId);
            
            $confirm = self::_db()->insert([
                "userId" => $userId,
                "token" => $token,
                "expiration" => date('c', $expiration)
            ]);

            if($confirm) return $token;
        }

        public static function delete(int $userId)
        {
            return self::_db()->delete([
                "where" => "userId = ?",
                "bind" => [$userId]
            ]); 
        }

        public static function validate(int $userId, string $token) : string
        {
            $validate = self::_db()->select([
                "where" => "userId = ? AND token = ?",
                "bind" => [$userId, $token]
            ]);    

            if(empty($validate->expiration)) return "invalid token";

            if(strtotime($validate->expiration) >=  CURRENT_TIME)
            { 
                #token validated
                self::delete($userId);

                return Constant::SUCCESS;
            }

            #Token Expired
            self::delete($userId);

            return "Token expired";
        }
    }