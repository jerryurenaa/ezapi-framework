<?php
    namespace Core;


    /**
    * @copyright (c) Nerdtrix LLC 2021
    * @author Name: Jerry Urena
    * @author Social links:  @jerryurenaa
    * @author email: jerryurenaa@gmail.com
    * @author website: jerryurenaa.com
    * @license MIT (included with this project)
    * @comment Define any important variable here 
    * so it can be used across the application to prevent duplications.
    */

    
    class Config
    {
        public static function load() : void
        {
            $srcDirectory = __DIR__;
            $SLASH = DIRECTORY_SEPARATOR;

            #Directories
            define("SLASH", $SLASH);    #Dinamic slashes to prevent issues in windows computers
            define("ROOT_DIR", dirname($srcDirectory, 2));  #Go back 2 level from the current directory
            define("SRC_DIR", dirname($srcDirectory, 1));   #Src directory
            define("CORE_DIR", $srcDirectory);   #Core directory
            define("SYSTEM_DIR", "{$srcDirectory}{$SLASH}System");  #system directory

            #Essentials
            define("NEWLINE", PHP_EOL);
            define("TWO_NEWLINES", PHP_EOL . PHP_EOL);

            #Date & Timezone config
            define("CURRENT_TIMEZONE", "America/New_York");
            date_default_timezone_set(CURRENT_TIMEZONE);
            define("CURRENT_TIME", time()); //Unix timestamp in seconds
            define("CURRENT_DATE", date("Y-m-d"));
            define("TIMESTAMP", date("Y-m-d H:i:s")); 
            
            #Auth config
            define("ALLOW_MULTI_LOGIN", true);
            define("MULTI_LOGIN_COUNT", 6); //number of devices allowed to be logged at the same time
            define("DEBUGING_TOOLS", ["PostmanRuntime"]);//these tools will be blocked in production
            define("IP_BLACKLIST", ["10.0.0.0"]); //blocked ips in production mode
            define("USER_SESSION_NAME", "DNRJZJXQGAEPA0BMYN5Q"); //Auth session name
            define("USER_SESSION_EXPIRY", 15); //time in minutes. is the timout time for the cooke unless the session is extended
            define("USER_AGENT_NAME", "ezapi");
        
            #Route
            define("DEFAULT_ROUTE", "User"); //Default route
            
            #App config
            define("REQUIRED_PHPVERSION", "7.4"); //PHP version 7.4 or GREATER  is required
            define("PRODUCTION", false); //Set to true for production

            #Rest API settings
            define("ALLOW_ANY_API_CONNECTION", true); 
            define("ALLOWED_ORIGINS", [
                "http://localhost",
                "::1",
                "127.0.0.1",
                "http://localhost:3000"
            ]); 

        }
    }