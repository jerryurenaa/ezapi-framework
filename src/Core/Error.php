<?php
  namespace Core;
  use Core\Request;
  use Core\Constant;

   /**
  * @copyright (c) Nerdtrix LLC 2021
  * @author Name: Jerry Urena
  * @author Social links:  @jerryurenaa
  * @author email: jerryurenaa@gmail.com
  * @author website: jerryurenaa.com
  * @license MIT (included with this project)
  * @comment Error class
  */


  class Error 
  {

    /**
     * @method exceptionHandler
     * @param string ex is the exception error given
     * By default errors will be shown only in development mode.
     */
    public static function handler($ex)
    {
      $api = new Request();

      error_reporting(E_ALL);

      if (!PRODUCTION) 
      {
        $api->response(500, [
          'Message' => Constant::ERROR_MESSAGE,
          'Uncaught exception' => get_class($ex),
          'Exception message' => $ex->getMessage(),
          'Trace' => $ex->getTraceAsString(),
          'Thrown in' => $ex->getFile(),
          'On line' => $ex->getLine()
        ]);
      } 
      else 
      {
        ini_set('ignore_repeated_errors', TRUE);
        ini_set('display_errors', TRUE);
        ini_set('log_errors', TRUE);
        ini_set('error_log', sprintf("%s%sErrors%s%serror_log.txt", SYSTEM_DIR, SLASH, SLASH, TIMESTAMP));

        $log = Constant::ERROR_MESSAGE;
        $log .= "<pre>Uncaught exception: " . get_class($ex);
        $log .= "<pre>Exception message  {$ex->getMessage()}";
        $log .= "<pre>Trace: {$ex->getTraceAsString()}";
        $log .= "<pre>Thrown in {$ex->getFile()}";
        $log .= "<pre>On line {$ex->getLine()}\n";
       
        error_log($log);

        $api->response(500, "Internal server error"); 
      }
    }
  }
